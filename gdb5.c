#include <stdio.h>

void g() {
  puts("In g()");
}

void f() {
  puts("In f()");
  g();
}

int main() {
  f();
}
