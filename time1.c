#include <stdio.h>
#include <time.h>

// Download https://web.cs.dal.ca/~vlado/pl/C_Standard_2011-n1570.pdf
// time.h is discussed in section 7.27

int main()
{
  clock_t c;
  time_t t = time(NULL);

  c = clock();
  printf("%ld %ld \n", c, CLOCKS_PER_SEC);

  c = clock();
  printf("%ld %ld \n", c, c/CLOCKS_PER_SEC);

  c = clock();
  printf("%ld %ld \n", c, c/CLOCKS_PER_SEC);

  c = clock();
  printf("%ld %ld \n", c, c/CLOCKS_PER_SEC);

  c = clock();
  printf("%ld %ld \n", c, c/CLOCKS_PER_SEC);

  c = clock();
  printf("%ld %ld \n", c, c/CLOCKS_PER_SEC);

  c = clock();
  printf("%ld %ld \n", c, c/CLOCKS_PER_SEC);

  printf("%ld\n", t);
  
  return 0;
}
