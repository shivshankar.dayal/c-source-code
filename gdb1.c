#include <stdio.h>

void f() {
  for(int j=0; j<5; ++j) {
    printf("%d\n", j);
  }
}

int main()
{
  for(int i=0; i<5; ++i) {
    printf("%d\n", i);
  }
  f();
  
  return 0;
}
