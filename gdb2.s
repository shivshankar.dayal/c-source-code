	.file	"gdb2.c"
	.text
	.globl	MIN
	.section	.rodata
	.align 4
	.type	MIN, @object
	.size	MIN, 4
MIN:
	.long	5
.LC0:
	.string	"%d\n"
.LC1:
	.string	"i is zero."
.LC2:
	.string	"i is not zero."
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	$0, -12(%rbp)
	jmp	.L2
.L3:
	movl	-12(%rbp), %eax
	movl	%eax, %esi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	addl	$1, -12(%rbp)
.L2:
	cmpl	$4, -12(%rbp)
	jle	.L3
	movl	$0, -8(%rbp)
	jmp	.L4
.L5:
	movl	-8(%rbp), %eax
	movl	%eax, %esi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	addl	$1, -8(%rbp)
.L4:
	movl	$5, %eax
	cmpl	%eax, -8(%rbp)
	jl	.L5
	movl	$0, -4(%rbp)
	movl	$0, -4(%rbp)
	cmpl	$0, -4(%rbp)
	je	.L6
	leaq	.LC1(%rip), %rdi
	call	puts@PLT
	jmp	.L7
.L6:
	leaq	.LC2(%rip), %rdi
	call	puts@PLT
.L7:
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 7.3.0-16ubuntu3) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
