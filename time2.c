#include <stdio.h>
#include <time.h>

static const char *const wday[] = {
  "Sunday", "Monday", "Tuesday", "Wednesday",
  "Thursday", "Friday", "Saturday", "-unknown-"
};

int main() {
  struct tm time_str;

  int year, month, date;
  
  printf("Enter year(>=1900), month(1 for January, 2 for February and so on), day of the month:\n");
  scanf("%d %d %d", &year, &month, &date);
  
  time_str.tm_year = year - 1900; // year since 1900
  time_str.tm_mon = month - 1;    // month since January [0 - 11]
  time_str.tm_mday = date;        // date of the month
  time_str.tm_hour = 0;           // 24 hour format 0 -23
  time_str.tm_min = 0;            // minutes 0 - 59
  time_str.tm_sec = 1;            // seconds 0 - 59
  time_str.tm_isdst = -1;         // daylight saving time

  if (mktime(&time_str) == (time_t)(-1))
    time_str.tm_wday = 7;

  printf("%s %d\n", wday[time_str.tm_wday], time_str.tm_yday);

  return 0;
}
